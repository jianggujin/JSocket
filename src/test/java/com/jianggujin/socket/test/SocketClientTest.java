package com.jianggujin.socket.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.SocketException;

import org.junit.Test;

import com.jianggujin.socket.JSocketClient;

public class SocketClientTest {
   @Test
   public void test() throws SocketException, IOException {
      JSocketClient client = new JSocketClient();
      client.connect("www.baidu.com", 80);
      InputStream input = client.getInputStream();
      OutputStream out = client.getOutputStream();
      out.write("GET http://www.baidu.com/ HTTP/1.1\r\n".getBytes("UTF-8"));
      out.write("Accept-Encoding: gzip,deflate\r\n".getBytes("UTF-8"));
      out.write("Host: www.baidu.com\r\n".getBytes("UTF-8"));
      out.write("Connection: Keep-Alive\r\n".getBytes("UTF-8"));
      out.write("User-Agent: SocketClient\r\n\r\n".getBytes("UTF-8"));
      BufferedReader reader = new BufferedReader(new InputStreamReader(input));
      String line = null;
      // 只读取响应头信息
      while ((line = reader.readLine()) != null && line.length() > 0) {
         System.out.println(line);
      }
      reader.close();
      client.close();
   }
}
