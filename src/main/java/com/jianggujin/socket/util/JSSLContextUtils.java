/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.socket.util;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

public class JSSLContextUtils {

   private JSSLContextUtils() {
   }

   public static SSLContext createSSLContext(String protocol, KeyManager keyManager, TrustManager trustManager)
         throws IOException {
      return createSSLContext(protocol, keyManager == null ? null : new KeyManager[] { keyManager },
            trustManager == null ? null : new TrustManager[] { trustManager });
   }

   public static SSLContext createSSLContext(String protocol, KeyManager[] keyManagers, TrustManager[] trustManagers)
         throws IOException {
      SSLContext ctx;
      try {
         ctx = SSLContext.getInstance(protocol);
         ctx.init(keyManagers, trustManagers, /* SecureRandom */ null);
      } catch (GeneralSecurityException e) {
         IOException ioe = new IOException("Could not initialize SSL context");
         ioe.initCause(e);
         throw ioe;
      }
      return ctx;
   }
}
